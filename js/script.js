//offer toggle
const offer1 = document.getElementById('offer-1');
const offer2 = document.getElementById('offer-2');
const continueBtn = document.getElementById('continue-btn');
const linkApple = 'https://apple.com/';
const linkGoogle = 'https://google.com/';

const offerToggle = (event) => {
  const currentElement = event.currentTarget;

  if(offer1 === currentElement){
    offer1.classList.remove('offer--disabled');
    offer2.classList.add('offer--disabled');
    continueBtn.href = linkApple;
  }
  if(offer2 === currentElement){
    offer1.classList.add('offer--disabled');
    offer2.classList.remove('offer--disabled');
    continueBtn.href = linkGoogle;
  }

}
offer1.addEventListener('click',(e) => offerToggle(e));
offer2.addEventListener('click',(e) => offerToggle(e));

//language toggle
import { en } from '../assets/localizations/en.js';
import { es } from '../assets/localizations/es.js';
import { fr } from '../assets/localizations/fr.js';
import { ja } from '../assets/localizations/ja.js';
import { nl } from '../assets/localizations/nl.js';
import { ru } from '../assets/localizations/ru.js';
import { zh } from '../assets/localizations/zh.js';

const languageList = { en, es, fr, ja, nl, ru, zh };

const getUserLanguage = () => {

  let searchParamLanguage = "";
  let userSystemLanguage = "";
  const PARAM_LENGTH = 8;

  const searchParam = window.location.search.toLowerCase();

  if (searchParam.length === PARAM_LENGTH && searchParam.slice(0, 6) === "?lang=") {
    searchParamLanguage = searchParam.slice(6, 8);
  }

  userSystemLanguage = window.navigator
    ? (window.navigator.language
      || window.navigator.systemLanguage
      || window.navigator.userLanguage).toLowerCase().slice(0, 2)
    : "en";
    
  return searchParamLanguage ? searchParamLanguage : userSystemLanguage; 
};

const contentLanguage = getUserLanguage();
let languageData = {}

for (let key in languageList) {
  if(key === contentLanguage){
    languageData = {...languageList[key]}
  }
};

const dataAttributes = document.querySelectorAll(`[data-content]`);

dataAttributes.forEach(element => {
  element.innerHTML = languageData[element.dataset.content];
});

//title size
const title = document.querySelector('.title');
const TITLE__LENGTH = 35;
if(title.textContent.length >= TITLE__LENGTH){
  title.classList.add("title__size");
}else{
  title.classList.remove("title__size");
};







